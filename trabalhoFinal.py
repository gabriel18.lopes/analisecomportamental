# Gabriel Lopes Silva
# gabriel18.lopes@gmail.com

import pandas as pd
import os
import csv
import matplotlib.pyplot as plt
import random

# filtrando apenas as ações que representam mais de 2%

#    LREN3	2,199
#    JBSS3	2,314
#    ITSA4	3,335
#    BBAS3	3,537
#    ABEV3	4,127
#    PETR3	5,092
#    B3SA3	5,475
#    BBDC4	7,033
#    PETR4	7,210
#    VALE3	8,054
#    ITUB4	9,204

caminho_do_arquivo ='/home/gabriel/Dropbox/TCC/Cotacoes/2015-2018/ComCVS/A3/2015-2018_Agrupadas3Dias3pv.csv'

acoes = ["ITUB4       ","VALE3       ","PETR4       ","BBDC4       ",
     "B3SA3       ","PETR3       ","ABEV3       ","BBAS3       ",
     "ITSA4       ","JBSS3       ","LREN3       "]

arq = pd.read_csv(caminho_do_arquivo)

dados = arq[arq.CODNEG == acoes[0]]

for i in range(1,11):
   dados = dados.append(arq[arq.CODNEG == acoes[i]])
       
# resetando index
dados = dados.reset_index(drop=True)
dados = dados.drop([dados.columns[0]],axis=1)
       
#histograma
arq = dados['PORC']
arq.hist(histtype = 'stepfilled', bins = 2000)
plt.xlim(xmin = -0.11,xmax=0.11)
plt.ylim(ymax = 50)    
 

# Classificando variacoes

# estavel (-0.005,0.005)
# variacao_baixa (-0.01,0.01)
# variacao_moderada (-0.025,0.025)
# variacao_alta (-0.05,0.05)
# variacao_anormal (resto)
# subindo
# descendo 

dados['estavel'] = dados.index.isin( dados[(dados.PORC > (-0.005))][(dados.PORC < (0.005))].index )
dados['variacao_baixa'] = dados.index.isin( dados[(dados.PORC > (-0.01))][(dados.PORC < (0.01))][dados.estavel == 0].index )
dados['variacao_moderada'] = dados.index.isin( dados[(dados.PORC > (-0.025))][(dados.PORC < (0.025))][dados.variacao_baixa == 0].index )
dados['variacao_alta'] = dados.index.isin( dados[(dados.PORC > (-0.05))][(dados.PORC < (0.05))][dados.variacao_moderada == 0].index )
dados['variacao_anormal'] = dados.index.isin( dados[dados.estavel == 0][dados.variacao_baixa == 0][dados.variacao_moderada == 0][dados.variacao_alta == 0].index )
dados['subindo'] = dados.index.isin( dados[(dados.PORC > (0.005))].index )
dados['descendo'] = dados.index.isin( dados[(dados.PORC > (-0.005))].index )

LREN3 = dados[ dados.CODNEG == acoes[10] ]
JBSS3 = dados[ dados.CODNEG == acoes[9] ]
ITSA4 = dados[ dados.CODNEG == acoes[8] ]
BBAS3 = dados[ dados.CODNEG == acoes[7] ]
ABEV3 = dados[ dados.CODNEG == acoes[6] ]
PETR3 = dados[ dados.CODNEG == acoes[5] ]
##B3SA3 = dados[ dados.CODNEG == acoes[4] ]
BBDC4 = dados[ dados.CODNEG == acoes[3] ]
PETR4 = dados[ dados.CODNEG == acoes[2] ]
VALE3 = dados[ dados.CODNEG == acoes[1] ]
ITUB4 = dados[ dados.CODNEG == acoes[0] ]

# acao 1, acao2 , 2 subindo, 2 descendo, oposto 
comparacao = pd.DataFrame(columns = ['stock1','stock2','up','down','oposit'])

#controla acao1
k = 0
for k in range(0,11):
    #controla a acao2 
    o = k + 1
    if((o!=12)&(k!=4)&(o!=4)):
        for j in range(o,11):
            if(j!=4):
                upp = 0
                downn = 0
                opositt = 0
                
                for i in range(0,311):
                    stock1 = dados[ dados.CODNEG == acoes[k] ]
                    stock2 = dados[ dados.CODNEG == acoes[j] ]
                    # resetando index
                    stock1 = stock1.reset_index(drop=True)
                    stock1 = stock1.drop([stock1.columns[0]],axis=1)
                    stock2 = stock2.reset_index(drop=True)
                    stock2 = stock2.drop([stock2.columns[0]],axis=1)
                    
                    
                    if( stock1.subindo[i] == stock2.subindo[i] ):
                        # ambas subindo
                        upp = upp +1
                    elif ( stock1.descendo[i] == stock2.descendo[i] ):
                        # ambas descendo
                        downn = downn + 1
                    elif ( stock1.subindo[i] == stock2.descendo[i] ):
                        # opostas
                        opositt = opositt + 1
                        
                comparacao = comparacao.append(
                        pd.DataFrame(
                        {"stock1": acoes[k],
                         "stock2": acoes[j],
                         "up":[upp],
                         "down":[downn],
                         "oposit":[opositt]}
                        )
                )
                
    



